# frozen_string_literal: true

require 'curb'

# methods to curl a url
module Curlable
  RETRIES = 5

  class BadHttpResponse < StandardError; end

  def curl_get(url, opts = {})
    c = Curl::Easy.new(url) { |curl| configure(curl, opts[:headers] || {}) }
    try(c, opts[:retries] || RETRIES)
    c.perform
    code = c.response_code
    raise BadHttpResponse, "#{code} at #{url}" unless [2, 3].include? code / 100

    c.body_str
  end

  private

  def try(curl, tries)
    curl.perform
    curl.on_failure { |c, _e| try(c, tries -= 1) if tries > 1 } # recurse
  end

  def configure(curl, headers)
    curl.follow_location = true
    curl.encoding = 'gzip'
    headers.each { |k, v| curl.headers[k.capitalize] = v }
  end
end

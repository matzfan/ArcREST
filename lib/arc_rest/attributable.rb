# frozen_string_literal: true

# adds ability to dynamically set instance vars & accessors
module Attributable
  def create_method(name, &)
    self.class.send(:define_method, name.to_sym, &)
  end

  def create_setter(method)
    create_method("#{method}=") { |v| instance_variable_set("@#{method}", v) }
  end

  def create_getter(method)
    create_method(method.to_sym) { instance_variable_get("@#{method}") }
  end

  def set_attr(method, value)
    create_setter(method)
    send "#{method}=", value
    create_getter(method)
  end
end

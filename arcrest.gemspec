# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'arc_rest/version'

Gem::Specification.new do |spec|
  spec.name          = 'arcrest'
  spec.version       = ArcREST::VERSION
  spec.authors       = ['matzfan']
  spec.summary       = 'Wrapper for ArcGIS REST API'
  spec.description   = 'Wrapper for ArcGIS REST API'
  spec.homepage      = 'https://gitlab.com/matzfan/arcrest'
  spec.required_ruby_version = '>= 3.3.3'
  spec.license = 'MIT'

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']
  spec.metadata = { 'rubygems_mfa_required' => 'true' }

  spec.add_runtime_dependency 'curb', '~> 1.0'
end

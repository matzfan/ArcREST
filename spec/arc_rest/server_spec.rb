# frozen_string_literal: true

describe ArcREST::Server do
  let(:url) { 'https://sampleserver6.arcgisonline.com/ArcGIS/rest/services' }
  let(:proxied_url) { PROXIED_ENDPOINT }
  let(:bad_server_url) { ArcREST::Catalog.new('bad/url') }
  let(:server) { described_class.new url }
  let(:proxied_server) { described_class.new(proxied_url, headers: { referer: REFERER }) }

  describe '#new(url)' do
    context 'with a valid endpoint url address' do
      it 'returns an instance of the class' do
        expect(server.class).to eq described_class
      end
    end

    context 'with an invalid endpoint address' do
      it 'raises ArgumentError "Invalid ArcGIS endpoint"' do
        expect { bad_server_url }.to raise_error ArgumentError, 'Invalid ArcGIS endpoint'
      end
    end
  end

  describe '#json' do
    it 'returns a hash of server data' do
      expect(server.json.class).to eq Hash
    end
  end

  describe '#version' do
    it 'returns the server version number' do
      expect(proxied_server.version).to eq 10.71
    end
  end
end

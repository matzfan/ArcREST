# frozen_string_literal: true

feature_service = 'https://sampleserver6.arcgisonline.com/ArcGIS/rest/services/Military/FeatureServer'
map_service = 'https://rmgsc.cr.usgs.gov/arcgis/rest/services/gmeelevation/MapServer'
proxied_feature_service = "#{PROXIED_ENDPOINT}/Historic_Buildings/Historic_Buildings/FeatureServer"

describe ArcREST::Service do
  let(:ms) { described_class.new(map_service) }
  let(:pfs) { described_class.new(proxied_feature_service, headers: { referer: REFERER }) }
  let(:fs) { described_class.new(feature_service) }

  describe '#new(url)' do
    it 'can be instantiated with a url string' do
      expect(described_class.new(feature_service).class).to eq described_class
    end
  end

  describe '#layers' do
    it 'returns a list of layers hashes which include "id" and "name" keys' do
      ms.layers.map(&:keys).each do |keys|
        expect(%w[id name].all? { |k| keys.include? k }).to be true
      end
    end
  end

  describe '#max_record_count' do
    it 'returns the maxRecordCount (API feature query limit) for a MapServer' do
      expect(ms.max_record_count).to eq 2000
    end

    it 'returns the maxRecordCount (API feature query limit) for a FeatureServer' do
      expect(fs.max_record_count).to eq 1000
    end
  end

  describe '#capabilities' do
    it 'returns a list of capabilities for a FeatureServer' do
      expect(fs.capabilities).to eq %w[Query Create Update Delete Uploads Editing]
    end

    it 'returns a list of capabilities for a proxied FeatureServer' do
      expect(pfs.capabilities).to eq %w[Query]
    end

    it 'returns a list of capabilities for a MapServer' do
      expect(ms.capabilities).to eq %w[Map Query Data]
    end
  end
end

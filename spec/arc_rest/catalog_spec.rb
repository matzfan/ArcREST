# frozen_string_literal: true

catalog_url = 'http://sampleserver6.arcgisonline.com/arcgis/rest/services'
proxied_url = PROXIED_ENDPOINT

describe ArcREST::Catalog do
  let(:catalog) { described_class.new(catalog_url) }
  let(:proxied_catalog) { described_class.new(proxied_url, headers: { referer: REFERER }) }

  describe '#new(url)' do
    it 'can be instantiated with a url string' do
      expect(catalog.class).to eq described_class
    end
  end

  describe '#folders' do
    it 'returns a list of sub-folder names' do
      expect(catalog.folders).to eq %w[AGP Elevation Energy LocalGovernment Locators NetworkAnalysis Oblique
                                       OsoLandslide ScientificData SpatioTemporalAggregation StoryMaps Sync Utilities]
    end
  end

  describe '#services' do
    it 'returns a list of services available' do
      expect(catalog.services[4]).to eq({ 'name' => 'CharlotteLAS', 'type' => 'ImageServer' })
    end
  end
end

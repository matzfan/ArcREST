# frozen_string_literal: true

require 'spec_helper'

# rubocop:disable Rspec/SpecFilePathFormat
describe ArcREST do
  it 'has a gem version number' do
    expect(ArcREST::VERSION).not_to be_nil
  end
end
# rubocop:enable Rspec/SpecFilePathFormat
